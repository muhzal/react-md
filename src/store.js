import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { browserHistory } from "react-router";
import { syncHistoryWithStore, routerMiddleware } from "react-router-redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import freeze from "redux-freeze";
import reducers from './reducers';

const 	isDev 		= process.env.NODE_ENV !== 'production';
let 	middlewares = [ routerMiddleware(browserHistory), thunk ];

if (isDev) {
	middlewares.push(freeze);
}

let 	middleware 	= isDev 
						? composeWithDevTools(applyMiddleware(...middlewares))
						: applyMiddleware(...middlewares);
const 	store 		= createStore(reducers, middleware);
const 	history 	= syncHistoryWithStore(browserHistory, store);

export  { store, history };