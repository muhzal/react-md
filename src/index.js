import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import WebFontLoader from 'webfontloader';
import { store } from './store.js';
import router from './router.js';
import { Layout } from './containers';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

WebFontLoader.load({
  google: {
    families: ['Roboto:300,400,500,700', 'Material Icons'],
  },
});

ReactDOM.render(
	<Provider store={store}>
		<Layout/>
	</Provider>,
  	document.getElementById('root')
	);
registerServiceWorker();