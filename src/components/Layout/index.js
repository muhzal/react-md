import React from 'react';
import LeftMenu from '../../components/LeftMenu';
import AppTitle from './AppTitle';

export class Layout extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let { children, toolbarTitle, ...props} = this.props;

    return (
        <NavigationDrawer         
          navItems={this._navItems}
          defaultVisible={true}
          persistentIconChildren="close"
          includeDrawerHeader={false}
          // renderNode={dialog}
          // contentClassName="md-grid"
          // drawerHeaderChildren={drawerHeaderChildren}
          mobileDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}
          tabletDrawerType={NavigationDrawer.DrawerTypes.PERSISTENT}
          desktopDrawerType={NavigationDrawer.DrawerTypes.PERSISTENT}
          toolbarTitle={ toolbarTitle }
          // toolbarActions={closeButton}
          // contentId="main-content-demo"
        >
          { this.props.children }
         </NavigationDrawer>   
    );
  }
}
