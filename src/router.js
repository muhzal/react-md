import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Template, Home } from './containers';

// build the router
export default (
  <Router onUpdate={() => window.scrollTo(0, 0)} history={browserHistory}>
    <Route path="/" component={Template}>
      <IndexRoute component={Home}/>
    </Route>
  </Router>
);
